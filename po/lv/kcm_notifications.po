# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# SPDX-FileCopyrightText: 2024 Toms Trasuns <toms.trasuns@posteo.net>
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-03 00:39+0000\n"
"PO-Revision-Date: 2024-01-18 21:02+0200\n"
"Last-Translator: Toms Trasuns <toms.trasuns@posteo.net>\n"
"Language-Team: Latvian <kde-i18n-doc@kde.org>\n"
"Language: lv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : "
"2);\n"
"X-Generator: Lokalize 23.08.4\n"

#: kcm.cpp:73
#, kde-format
msgid "Toggle do not disturb"
msgstr "Ieslēgt/izslēgt netraucēšanas režīmu"

#: sourcesmodel.cpp:393
#, kde-format
msgid "Other Applications"
msgstr "Citas programmas"

#: ui/ApplicationConfiguration.qml:91
#, kde-format
msgid "Show popups"
msgstr "Rādīt uzlecošos paziņojumus"

#: ui/ApplicationConfiguration.qml:105
#, kde-format
msgid "Show in do not disturb mode"
msgstr "Rādīt netraucēšanas režīmā"

#: ui/ApplicationConfiguration.qml:118 ui/main.qml:147
#, kde-format
msgid "Show in history"
msgstr "Rādīt vēsturē"

#: ui/ApplicationConfiguration.qml:129
#, kde-format
msgid "Show notification badges"
msgstr "Rādīt paziņojumu nozīmītes"

#: ui/ApplicationConfiguration.qml:164
#, kde-format
msgctxt "@title:table Configure individual notification events in an app"
msgid "Configure Events"
msgstr "Konfigurēt notikumus"

#: ui/ApplicationConfiguration.qml:172
#, kde-format
msgid ""
"This application does not support configuring notifications on a per-event "
"basis"
msgstr ""
"Šī programma neatbalsta paziņojumu konfigurāciju katram gadījumam atsevišķi"

#: ui/ApplicationConfiguration.qml:263
#, kde-format
msgid "Show a message in a pop-up"
msgstr "Ziņojumu rādīt uzlecošā paziņojumā"

#: ui/ApplicationConfiguration.qml:272
#, kde-format
msgid "Play a sound"
msgstr "Atskaņot skaņu"

#: ui/main.qml:41
#, kde-format
msgid ""
"Could not find a 'Notifications' widget, which is required for displaying "
"notifications. Make sure that it is enabled either in your System Tray or as "
"a standalone widget."
msgstr ""
"Neizdevās atrast „Paziņojumu“ logdaļu, kas ir nepieciešama paziņojumu "
"rādīšanai. Pārliecinieties, ka tā ir pievienota ikonu joslai vai kā "
"atsevišķa logdaļa."

#: ui/main.qml:52
#, kde-format
msgctxt "Vendor and product name"
msgid "Notifications are currently provided by '%1 %2' instead of Plasma."
msgstr "Šobrīd paziņojumus nenodrošina „Plasma“, bet gan „%1 %2“."

#: ui/main.qml:56
#, kde-format
msgid "Notifications are currently not provided by Plasma."
msgstr "Šobrīd paziņojumus nenodrošina „Plasma“."

#: ui/main.qml:63
#, kde-format
msgctxt "@title:group"
msgid "Do Not Disturb mode"
msgstr "Netraucēšanas režīms"

#: ui/main.qml:68
#, kde-format
msgctxt "Automatically enable Do Not Disturb mode when screens are mirrored"
msgid "Enable automatically:"
msgstr ""

#: ui/main.qml:69
#, fuzzy, kde-format
#| msgctxt "Enable Do Not Disturb mode when screens are mirrored"
#| msgid "When screens are mirrored"
msgctxt "Automatically enable Do Not Disturb mode when screens are mirrored"
msgid "When screens are mirrored"
msgstr "Kad ir spoguļoti ekrāni"

#: ui/main.qml:81
#, fuzzy, kde-format
#| msgctxt "Enable Do Not Disturb mode during screen sharing"
#| msgid "During screen sharing"
msgctxt "Automatically enable Do Not Disturb mode during screen sharing"
msgid "During screen sharing"
msgstr "Ekrāna kopīgošanas laikā"

#: ui/main.qml:96
#, kde-format
msgctxt "Keyboard shortcut to turn Do Not Disturb mode on and off"
msgid "Manually toggle with shortcut:"
msgstr ""

#: ui/main.qml:103
#, kde-format
msgctxt "@title:group"
msgid "Visibility conditions"
msgstr "Rādīšanas nosacījumi"

#: ui/main.qml:108
#, kde-format
msgid "Critical notifications:"
msgstr "Kritiskie paziņojumi:"

#: ui/main.qml:109
#, kde-format
msgid "Show in Do Not Disturb mode"
msgstr "Rādīt netraucēšanas režīmā"

#: ui/main.qml:121
#, kde-format
msgid "Normal notifications:"
msgstr "Normālie paziņojumi:"

#: ui/main.qml:122
#, kde-format
msgid "Show over full screen windows"
msgstr "Rādīt virs pilnekrāna logiem"

#: ui/main.qml:134
#, kde-format
msgid "Low priority notifications:"
msgstr "Zemas prioritātes paziņojumi:"

#: ui/main.qml:135
#, kde-format
msgid "Show popup"
msgstr "Rādīt uzlecošo paziņojumu"

#: ui/main.qml:164
#, kde-format
msgctxt "@title:group As in: 'notification popups'"
msgid "Popups"
msgstr "Uzlecošie paziņojumi"

#: ui/main.qml:170
#, kde-format
msgctxt "@label"
msgid "Location:"
msgstr "Atrašanās vieta:"

#: ui/main.qml:171
#, kde-format
msgctxt "Popup position near notification plasmoid"
msgid "Near notification icon"
msgstr "Pie paziņojuma ikonas"

#: ui/main.qml:208
#, kde-format
msgid "Choose Custom Position…"
msgstr "Izvēlēties pielāgotu rādīšanās vietu..."

#: ui/main.qml:217 ui/main.qml:233
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 sekundes"
msgstr[1] "%1 sekundēm"
msgstr[2] "%1 sekundēm"

#: ui/main.qml:222
#, kde-format
msgctxt "Part of a sentence like, 'Hide popup after n seconds'"
msgid "Hide after:"
msgstr "Paslēpt pēc:"

#: ui/main.qml:245
#, kde-format
msgctxt "@title:group"
msgid "Additional feedback"
msgstr "Papildu informēšana"

#: ui/main.qml:251
#, kde-format
msgctxt "Show application jobs in notification widget"
msgid "Show in notifications"
msgstr "Rādīt paziņojumos"

#: ui/main.qml:252
#, kde-format
msgid "Application progress:"
msgstr "Programmas progress:"

#: ui/main.qml:266
#, kde-format
msgctxt "Keep application job popup open for entire duration of job"
msgid "Keep popup open during progress"
msgstr "Progresa laikā uzlecošo logu nepaslēpt"

#: ui/main.qml:279
#, kde-format
msgid "Notification badges:"
msgstr "Paziņojumu nozīmītes:"

#: ui/main.qml:280
#, kde-format
msgid "Show in task manager"
msgstr "Rādīt uzdevumu pārvaldniekā"

#: ui/main.qml:291
#, kde-format
msgctxt "@title:group"
msgid "Application-specific settings"
msgstr "Programmu iestatījumi"

#: ui/main.qml:296
#, kde-format
msgid "Configure…"
msgstr "Konfigurēt..."

#: ui/PopupPositionPage.qml:14
#, kde-format
msgid "Popup Position"
msgstr "Uzlecošā paziņojuma vieta"

#: ui/SourcesPage.qml:19
#, kde-format
msgid "Application Settings"
msgstr "Programmu iestatījumi"

#: ui/SourcesPage.qml:99
#, kde-format
msgid "Applications"
msgstr "Programmas"

#: ui/SourcesPage.qml:100
#, kde-format
msgid "System Services"
msgstr "Sistēmas servisi"

#: ui/SourcesPage.qml:148
#, kde-format
msgid "No application or event matches your search term"
msgstr "Meklēšanas vārdiem neatbilda neviena programma un notikums"

#: ui/SourcesPage.qml:171
#, kde-format
msgid ""
"Select an application from the list to configure its notification settings "
"and behavior"
msgstr ""
"Sarakstā atlasiet programmu, lai konfigurētu tās paziņojumu iestatījumus un "
"uzvedību"

#~ msgctxt "Enable Do Not Disturb mode when screens are mirrored"
#~ msgid "Enable:"
#~ msgstr "Ieslēgt:"

#~ msgctxt "Keyboard shortcut to turn Do Not Disturb mode on and off"
#~ msgid "Keyboard shortcut:"
#~ msgstr "Tastatūras saīsne:"
